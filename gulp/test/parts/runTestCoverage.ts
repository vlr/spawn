
import { spawn } from "../../tools/spawn";
import { testJsFiles } from "../../settings";
import { settings } from "../../settings";

// "--reporter=html"
export function runTestCoverage(): Promise<void> {
  return spawn("runTestCoverage", "nyc", "--reporter=text", "mocha", testJsFiles(settings.build));
}

