import { spawnIt } from "../../tools/spawn";

export async function installRequest(): Promise<void> {
  await spawnIt("npm i", "npm", ["install", "request", "request-promise-native", "--save-dev"], {});
}
